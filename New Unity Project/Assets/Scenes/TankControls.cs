﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankControls : MonoBehaviour
{
    public float speed = 1.5f;
    float rotSpeed = 180f;

    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += Vector3.down * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Quaternion rot = transform.rotation;
            float z = rot.eulerAngles.z;
            z -= Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;
            rot = Quaternion.Euler(0, 0, z);
            transform.rotation = rot;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Quaternion rot = transform.rotation;
            float z = rot.eulerAngles.z;
            z -= Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;
            rot = Quaternion.Euler(0, 0, z);
            transform.rotation = rot;
        }
    }
}